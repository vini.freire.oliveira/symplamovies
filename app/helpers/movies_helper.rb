module MoviesHelper
  def clear_genre(genre)
    genre.split('|').join(", ").html_safe
  end

end
