class MoviesController < ApplicationController
  before_action :set_movie, only: [:show]

  def index
    if params[:value] && params[:value].length > 0
      set_movies
    else
      @movies = Movie.all.paginate(page: params[:page], per_page: 30)
    end
  end

  def show
  end

  def count
    render json: {count: Movie.all.count}
  end

  def create
    movies = ParserService.new(movie_params[:csv]).perform
    redirect_to root_path, flash: {success: "The system is processing the file"}
  end

  private 

  def movie_params
    params.require(:movie).permit(:csv)
  end

  def set_movie
    @movie = Movie.find(params[:id])
  end

  def set_movies
    @movies = MovieService.new.perform(params[:key],params[:value])
  end

end
