class Movie
  include Mongoid::Document
  include Mongoid::Timestamps
  field :title, type: String
  field :duration, type: Integer
  field :director, type: String
  field :actor1, type: String
  field :actor2, type: String
  field :actor3, type: String
  field :genres, type: String
  field :imdb_link, type: String
  field :budget, type: String
  field :imdb_score, type: String
  field :imdb_reviews, type: String
  field :year, type: String
  field :language, type: String

  validates_presence_of :title
  validates_uniqueness_of :title
end
