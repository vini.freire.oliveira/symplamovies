$(document).on('ready turbolinks:load', function(){
  $('#csv').change(function(){ $("#csv-input").submit(); });
  $('select').formSelect();
  countMovies();
});

function countMovies(){

  let url = window.location.href.split("/")
  let baseUrl = url[0]+"/"+url[1]+"/"+url[2]+"/"
  var settings = {
    "async": true,
    "crossDomain": true,
    "url": baseUrl+"movies/count",
    "method": "GET",
    "headers": {
      "content-type": "application/json"
    }
  }
  
  setInterval(function(){
    $.ajax(settings).done(function (response) {
      $("#movies_count").html('').prepend("");
      $("#movies_count").prepend(response.count + " movies");
    });
  },500)

}