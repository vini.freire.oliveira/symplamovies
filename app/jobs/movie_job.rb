class MovieJob < ApplicationJob
  queue_as :movies

  def perform(movie)
    save(movie.split(","))
  end

  private

  def save movie
    Movie.create(title: movie[11], duration: movie[3], director: movie[1], actor1: movie[10], actor2: movie[6], actor3: movie[14], genres: movie[9], imdb_link: movie[17], budget: movie[22], imdb_score: movie[25], imdb_reviews: movie[12], year: movie[23], language: movie[19])
  end

end

