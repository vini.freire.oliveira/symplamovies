class ParserService

  def initialize file
      @path = file.path
  end

  def perform
      parse_file
  end

  private

  def parse_file
      File.open(@path,'r').each do |movie| 
        MovieJob.perform_later(movie) unless movie.match(/director_name/)
      end
  end

end
