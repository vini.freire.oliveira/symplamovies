class MovieService

  def perform filter,value
      filter filter,value
  end

  private

  def filter filter,value
    movies = Movie.all.select do |movie|
      case filter
      when "title"  
        movie if movie.title.downcase.match?(value.downcase)
      when "year"   
        movie if movie.year.to_i >= value.to_i 
      when "imdb_score"   
        movie if movie.imdb_score.to_f >= value.to_f  
      when "director"
        movie if movie.director.downcase.match?(value.downcase)
      when "actor"
        movie if movie.actor1.downcase.match?(value.downcase) or movie.actor2.downcase.match?(value.downcase) or movie.actor3.downcase.match?(value.downcase)
      when "genre"
        flag = false
        movie.genres.split('|').each do |genre|
          if genre.downcase.match?(value.downcase)
            flag = true
            break 
          end
        end
        movie if flag
      when "duration"
        movie if movie.duration.to_i <= value.to_i 
      end
    end
    movies
  end
  
end