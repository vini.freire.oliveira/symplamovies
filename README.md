* Sympla Movies

  #####  Ops

  - This system was developed using Docker.

  #####  System dependencies

  - Docker (<https://www.docker.com/>)
- Docker-compose
  
#####  Technologies
  
  - Ruby 2.5
- Rails 5.2
  - Postgres
- Redis
  
  #####  Gems
  
  - gem 'sidekiq' Simple, efficient background processing for Ruby.
- gem 'mongoid' is an ODM (Object-Document Mapper) framework for MongoDB in Ruby..
  - The following gems were used to help in the automated tests

    - rspec
    - capybara
    - ffaker
    - factory_bot_rails
    - The Rspec results is in **docs spec-results.html** 
  
    
  
  #####  Step by step to run the project in localhost using docker-compose

  1. Clone the project

     ```
   git clone https://gitlab.com/vini.freire.oliveira/symplamovies.git
     ```
  
  2. Build the containers and install all dependencies

     ```
   docker-compose run --rm app bundle install
     ```
  
  3. Now, create database and generate migrations:

     ```
   docker-compose run --rm app bundle exec rails db:create
     ```
  
  4. Start the server with

     ```
   docker-compose up
     ```



###  Links

#####  [My Git](https://gitlab.com/vini.freire.oliveira) 

#####  [My LikedIn](https://www.linkedin.com/in/vinicius-freire-b53507107/)