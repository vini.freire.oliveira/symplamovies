require "rails_helper"

RSpec.feature "Open the site" do

    scenario "and the user visit root_path" do

      30.times do
        FactoryBot.create(:movie)
      end

      visit root_path

      expect(page).to have_content("Movies")

      Movie.all.each do |movie|
        expect(page).to have_content(`"#{movie.title}"`)
      end

  end

end
