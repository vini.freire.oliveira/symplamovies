FactoryBot.define do
  factory :movie do
      title { FFaker::Movie.unique.title }
      duration { rand(1..140) }
      director { FFaker::Name.name }
      actor1 { FFaker::Name.name }
      actor2 { FFaker::Name.name }
      actor3 { FFaker::Name.name }
      genres { FFaker::Lorem.word }
      imdb_link { FFaker::Internet.http_url }
      budget { "2212121" }
      imdb_score { rand(2.5..5.0) }
      imdb_reviews { "2212121" }
      year { "2015" }
      language { "English" }
  end
end
