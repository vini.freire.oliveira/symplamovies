require 'rails_helper'
 
RSpec.describe "Parser", type: :request do
    
    describe "POST /create" do
        before do
            post "/movies", params: { movie: {csv: Rack::Test::UploadedFile.new("#{Rails.root}/public/movies.csv", 'movies/csv')} }
        end

        it "returns found" do
            expect(response.status).to eql(302)
        end

        context "analyzing the uploaded csv file in public path" do
            
          it "30 movies in the file" do
              amount = Movie.all.count
              expect(amount).to eql(30)
          end

        end

    end
 
end
