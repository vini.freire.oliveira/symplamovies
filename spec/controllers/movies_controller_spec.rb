require 'rails_helper'

RSpec.describe MoviesController, type: :controller do
  describe "GET #index" do
    before(:each) do
      10.times do
        FactoryBot.create(:movie)
      end
    end

    context "See all movies created" do
      before(:each) do
        get :index
      end
    end
    
    it "returns http success" do
      expect(response).to have_http_status(:success)
    end

    it "created all movies" do
      movies = Movie.all
      expect(movies.count).to eq(10)
    end
  end

end
