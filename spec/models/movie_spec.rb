require 'rails_helper'

RSpec.describe Movie, type: :model do
  it { is_expected.to be_mongoid_document }

  before(:each) do
    @movie = FactoryBot.create(:movie)
  end

  describe 'Fields' do
    it { is_expected.to have_fields(:title) }
  end

  describe 'Validations' do
    context 'title' do
        it 'should be unique' do
            movie = FactoryBot.create(:movie)
            movie.title = @movie.title
            expect(movie).to_not be_valid
        end

        it 'should not be blank' do
            @movie.title = nil
            expect(@movie).to_not be_valid
        end
    end
  end 

end
